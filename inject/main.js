window.rrApiOnReady = [];

const replaceBlock = async (result, callback) => {
  const parsedResult = JSON.parse(result);
  const blockId = parsedResult.Id;

  const response = await fetch(`/_rr/markups/${blockId}.html`);

  if (!response.ok) {
    callback(result);
    return;
  }

  parsedResult.Markup = await response.text();
  callback(JSON.stringify(parsedResult));
};

function init() {
  const oldMake = retailrocket.cors.make;

  retailrocket.cors.make = (url, method, headers, data, withCredentials, callback, errback) => {
    const adapterCallback = (result) => {
      if (url.includes('blockId=')) {
        replaceBlock(result, callback);
      }
    };

    return oldMake(url, method, headers, data, withCredentials, adapterCallback, errback);
  };
}

window.rrApiOnReady.push(init);

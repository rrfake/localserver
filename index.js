const bs = require('browser-sync').create();

bs.init({
  proxy: {
    target: 'https://zdravcity.ru/search/r_moscowregion/?order=Y&what=%D0%BA%D0%B0%D1%88%D0%B5%D0%BB%D1%8C',
    ws: true,
  },
  serveStatic: [{
    route: '/_rr',
    dir: './inject',
  }],
  https: true,
  cors: true,
  watch: true,
  snippetOptions: {
    rule: {
      match: /<head>/i,
      fn: (snippet, match) => (`${match} <script src="/_rr/main.js"></script> ${snippet}`),
    },
  },
});
